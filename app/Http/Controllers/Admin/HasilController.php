<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\NilaiAlternatif;
use App\Models\Alternatif;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HasilExport;

class HasilController extends BobotSubKriteriaController
{
    public function hasil()
    {
        $data = $this->hitungHasil();
        return view('perhitungan.hasil')
        ->with($data);
    }

    public function hitungHasil()
    {
        $alternatif = Alternatif::whereHas('nilai_alternatif')->get();
        $bobotKriteria = $this->bobot_kriteria();
        $matrixHasil = [];
        $nilaiAlternatif = [];
        $prioritasSubKriteria = [];
        $prioritasKriteria = [];
        $nilaiAlternatif = [];
        $subKriteria = [];

        if($bobotKriteria['matrix1']['lengkap']){

            $matrixHasil['header'] = $bobotKriteria['matrix1']['header'];
            $matrixHasil['prioritas'] = $bobotKriteria['matrix2']['prioritas'];
            $prioritasKriteria = $bobotKriteria['matrix2']['prioritas'];
            $nilaiAlternatif['header'] = $bobotKriteria['matrix1']['header'];

            $x = 0;
            foreach ($matrixHasil['header'] as $key => $value) {
                $bobotSubKkriteria = $this->bobot_sub_kriteria($key);
                $prioritasSubKriteria += $bobotSubKkriteria['matrix2']['prioritas_subkriteria'];
                $subKriteria += $bobotSubKkriteria['matrix1']['header'];
                $i=0;
                foreach ($bobotSubKkriteria['matrix2']['prioritas_subkriteria'] as $key2 => $value2) {
                    $matrixHasil['data'][$x]['prioritas'][$i] = $value2;
                    $matrixHasil['data'][$x]['header'][$i] = $bobotSubKkriteria['matrix1']['header'][$key2];
                    $i++;
                }
                $x++;
            }
            foreach ($alternatif as $key => $value) {
                $total = 0;
                foreach ($matrixHasil['header'] as $keyHeader => $valueHeader) {
                    $nilaiAlternatifDB = NilaiAlternatif::whereHas('sub_kriteria', function($q) use($keyHeader){
                        $q->where('id_kriteria', $keyHeader);
                    })->where('id_alternatif', $value->id)->first()->toArray();
                    $nilaiAlternatif['data'][$value->id]['nama'] = $value->nama;
                    $nilaiAlternatif['data'][$value->id]['nilai'][$keyHeader] = $nilaiAlternatifDB;
                    $nilaiAlternatif['data'][$value->id]['nilai'][$keyHeader]['nama'] = $subKriteria[$nilaiAlternatifDB['id_sub_kriteria']];
                    $prioritas_kriteria = $matrixHasil['prioritas'][$keyHeader];
                    $prioritas_subkriteria = $prioritasSubKriteria[$nilaiAlternatifDB['id_sub_kriteria']];
                    $hasil = $prioritas_kriteria * $prioritas_subkriteria;
                    $total += $hasil;
                    $nilaiAlternatif['data'][$value->id]['nilai'][$keyHeader]['hasil'] = $hasil;
                    $nilaiAlternatif['data'][$value->id]['total'] = $total;
                }
            }
            $rangking = array_column($nilaiAlternatif['data'], 'total');
            array_multisort($rangking, SORT_DESC, $nilaiAlternatif['data']);
        }

        return ['matrixHasil' => $matrixHasil,
                'nilaiAlternatif' => $nilaiAlternatif
            ];
    }
    public function cetak()
    {
        $data = $this->hitungHasil();
        return (new HasilExport ($data))->download('hasil-perangkingan.xlsx');
    }
}
