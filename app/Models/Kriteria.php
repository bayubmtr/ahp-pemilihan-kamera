<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Kriteria extends Model
{
    protected $fillable = [
        'id', 'nama'
    ];
    protected $table = "kriteria";
    public function sub_kriteria()
    {
        return $this->hasMany('App\Models\SubKriteria', 'id_kriteria', 'id');
    }
    public function bobot_kriteria()
    {
        return $this->hasMany('App\Models\BobotKriteria', 'id_kriteria_1', 'id');
    }
}
