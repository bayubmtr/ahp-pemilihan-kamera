<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">AHP <sup>KAMERA</sup></div>
    </a>

    <hr class="sidebar-divider my-0">

    <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
        <a class="nav-link" href="/">
            <i class="fa fa-tachometer"></i>
            <span>Dashboard</span></a>
    </li>

    <hr class="sidebar-divider">

    @if(auth()->user()->role == 'admin')
    <div class="sidebar-heading">
        Master Data
    </div>

    <li class="nav-item {{ Request::is('kriteria') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('kriteria.index') }}">
            <i class="fa fa-list"></i>
            <span>KRITERIA</span>
        </a>
    </li>
    <li class="nav-item {{ Request::is('alternatif') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('alternatif.index') }}">
            <i class="fa fa-list"></i>
            <span>ALTERNATIF</span>
        </a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Analisis
    </div>

    <li class="nav-item {{ Request::is('bobot_kriteria') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('bobot_kriteria.index') }}">
            <i class="fa fa-balance-scale"></i>
            <span>BOBOT KRITERIA</span>
        </a>
    </li>

    <li class="nav-item {{ Request::is('bobot_sub_kriteria') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('bobot_sub_kriteria.index') }}">
            <i class="fa fa-balance-scale"></i>
            <span>BOBOT SUB KRITERIA</span>
        </a>
    </li>

    <hr class="sidebar-divider">

    <div class="sidebar-heading">
        Hasil
    </div>

    <li class="nav-item {{ Request::is('hasil') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('hasil.index') }}">
            <i class="fa fa-check"></i>
            <span>HASIL</span>
        </a>
    </li>
    @else
    <li class="nav-item {{ Request::is('bobot_kriteria') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('bobot_kriteria.index') }}">
            <i class="fa fa-balance-scale"></i>
            <span>BOBOT KRITERIA</span>
        </a>
    </li>
    <li class="nav-item {{ Request::is('hasil') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('hasil.index') }}">
            <i class="fa fa-check"></i>
            <span>HASIL</span>
        </a>
    </li>
    @endif

    <hr class="sidebar-divider d-none d-md-block">

    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
