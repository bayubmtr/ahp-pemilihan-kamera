@extends('layouts.auth')

@section('content')
<div class="row">
    <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
    <div class="col-lg-6">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-2">Create New Password</h1>
                <p class="mb-4">You are only one step a way from your new password, recover your password now.</p>
            </div>
            <form class="user" action="{{ route('password.update') }}" method="POST">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    <input name="email" type="email" class="form-control form-control-user @error('email') is-invalid @enderror"
                        id="exampleInputEmail" aria-describedby="emailHelp" value="{{ $email ?? old('email') }}"
                        placeholder="Enter Email Address..." required readonly>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input name="password" type="password" class="form-control form-control-user @error('password') is-invalid @enderror"
                        id="exampleInputPassword" aria-describedby="emailHelp"
                        placeholder="Enter Password Address...">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input name="password_confirmation" type="password" class="form-control form-control-user @error('password_confirmation') is-invalid @enderror"
                        id="exampleInputPassword_confirmation" aria-describedby="emailHelp"
                        placeholder="Re-Enter Password Address...">
                    @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <button class="btn btn-primary btn-user btn-block">
                    Reset Password
                </button>
            </form>
            <hr>
            <div class="text-center">
                <a class="small" href="{{ route('register') }}">Create an Account!</a>
            </div>
            <div class="text-center">
                <a class="small" href="{{ route('login') }}">Already have an account? Login!</a>
            </div>
        </div>
    </div>
</div>
@endsection
