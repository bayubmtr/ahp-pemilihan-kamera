@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">BOBOT KRITERIA</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Bobot Kriteria</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="card">
        <div class="card-header">
            <h3 class="m-0 text-dark"><strong>SKALA KEPENTINGAN</strong></h3>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered">
                <thead>
                    <tr>
                        @foreach ($skala_kepentingan as $key => $skl)
                        <th>{{$key}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        @foreach ($skala_kepentingan as $skl)
                        <td>{{$skl}}</td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card">
		<div class="card-header">
			<h3 class="m-0 text-dark"><strong>LANGKAH 1</strong>  MATRIKS PERBANDINGAN BERPASANGAN</h3>
		</div>
		<div class="card-body">
			<table id="example1" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Kriteria</th>
                        @foreach ($matrix1['header'] as $key => $header)
                        <th>{{$header}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach ($matrix1['data'] as $key => $value)
                    <tr>
                        <td>{{$matrix1['header'][$key]}}</td>
                        @foreach ($value as $key2 => $value2)
                            @if ($value2)
                            <td data-href='/bobot_kriteria/{{$key}},{{$key2}}/edit' class="clickable-row pointer bg-success">
                            {{$matrix1['data'][$key][$key2]}}
                            </td>
                            @else
                            <td data-href='/bobot_kriteria/{{$key}},{{$key2}}/edit' class="clickable-row pointer bg-danger">
                            null
                            </td>
                        @endif
                        @endforeach
                    </tr>
                    @endforeach
                    <tr>
                    <td class="bg-secondary">Jumlah</td>
                        @foreach ($matrix1['jumlah'] as $var)
                        <td class="bg-secondary">
                            {{$var}}
                        </td>
                        @endforeach
                    </tr>
                </tbody>
			</table>
		</div>
    </div>
    @if ($matrix1['lengkap'])
    <div class="card">
		<div class="card-header">
			<h3 class="m-0 text-dark"><strong>LANGKAH 2</strong>  MATRIKS NILAI KRITERIA</h3>
		</div>
		<div class="card-body">
			<table id="example1" class="table table-bordered table-striped">
			<thead>
                <th>Kriteria</th>
                @foreach ($matrix2['header'] as $value)
                <th>{{$value}}</th>
                @endforeach
                <th>Jumlah</th>
                <th>Prioritas</th>
			</thead>
			<tbody>
			    @foreach ($matrix2['data'] as $key => $value)
				<tr>
				    <td>{{ $matrix2['header'][$key] }}</td>
                    @foreach ($value as $key2 => $value2)
                    <td>{{ round($value2, 2) }}</td>
                    @endforeach
                    <td>{{ round($matrix2['jumlah'][$key], 2) }}</td>
                    <td>{{ round($matrix2['prioritas'][$key], 2) }}</td>
                </tr>
			    @endforeach
			</tbody>
			</table>
		</div>
    </div>
    <div class="card">
		<div class="card-header">
			<h3 class="m-0 text-dark"><strong>LANGKAH 3</strong>  MATRIKS PENJUMLAHAN SETIAP BARIS</h3>
		</div>
		<div class="card-body">
			<table id="example1" class="table table-bordered table-striped">
                <thead>
                    <th>Kriteria</th>
                    @foreach ($matrix3['header'] as $value)
                    <th>{{$value}}</th>
                    @endforeach
                    <th>Jumlah</th>
                </thead>
                <tbody>
                    @foreach ($matrix3['data'] as $key => $value)
                    <tr>
                        <td>{{ $matrix3['header'][$key] }}</td>
                        @foreach ($value as $key2 => $value2)
                        <td>{{ round($value2, 2) }}</td>
                        @endforeach
                        <td>{{ round($matrix3['jumlah'][$key], 2) }}</td>
                    </tr>
                    @endforeach
                </tbody>
			</table>
		</div>
    </div>
    <div class="card">
		<div class="card-header">
			<h3 class="m-0 text-dark"><strong>LANGKAH 4</strong>  PERHITUNGAN RASIO KONSISTENSI</h3>
		</div>
		<div class="card-body">
			<table id="example1" class="table table-bordered table-striped">
                <thead>
                    <th>Kriteria</th>
                    <th>Jumlah per Baris</th>
                    <th>Prioritas</th>
                    <th>Hasil</th>
                </thead>
                <tbody>
                    @foreach ($langkah4['data'] as $key => $value)
                    <tr>
                        <td>{{ $langkah4['header'][$key] }}</td>
                        <td>{{ round($value['jumlah'], 2) }}</td>
                        <td>{{ round($value['prioritas'], 2) }}</td>
                        <td>{{ round($value['hasil'], 2) }}</td>
                    </tr>
                    @endforeach
                </tbody>
			</table>
			<table id="example1" class="table table-bordered table-striped mt-3">
                <thead>
                    <th class="text-center" colspan="2">Keterangan</th>
                </thead>
                <tbody>
                    <tr>
                        <td>Jumlah</td>
                        <td>{{round($langkah5['jumlah'], 2)}}</td>
                    </tr>
                    <tr>
                        <td>n</td>
                        <td>{{round($langkah5['n'], 2)}}</td>
                    </tr>
                    <tr>
                        <td>λ Maks</td>
                        <td>{{round($langkah5['lamda'], 2)}}</td>
                    </tr>
                    <tr>
                        <td>CI</td>
                        <td>{{round($langkah5['ci'], 2)}}</td>
                    </tr>
                    <tr>
                        <td>CR</td>
                        <td>{{round($langkah5['cr'], 2)}}</td>
                        <td>{{$langkah5['konsisten']}}</td>
                    </tr>
                </tbody>
			</table>
		</div>
    </div>
    @endif
</section>
@include ('includes.script')
<script>
  jQuery(document).ready(function($) {
      $(".clickable-row").click(function() {
          window.location = $(this).data("href");
      });
  });
</script>
@endsection
